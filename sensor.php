<?php
  session_start();
  spl_autoload_register(function ($class_name) {
      include './include/' . $class_name . '.php';
  });

  include './include/functions.php';
  include './include/variables.php';

  if(isset($_GET['sid'])) {
    $sensor = new Sensor($_GET['sid']);
  }

  if(!$sensor->public) {
    if(!isset($_SESSION['user'])){
      header('Location: ./login.php');
    }
  }

  //$user = new User($_SESSION['user']);

  //$datatable = $sensor->getSensorLastValueList(288);

  
  $sensor->getSensorCurrentData();
  $sensor_output = '<h3 class="sensor-name">' . $sensor->getName();
  if(isLoggedUser()){
    $sensor_output .= ' <img src="./images/icons/edit-icon.png" id="edit-icon" />';
  }

  $sensor_output .= '</h3>';

  $sensor_output .= '<p class="last-updated">Last updated on ' . date('j.n H:i', $sensor->getLastUpdatedTime()) . '</p><div class="data-container">';
  foreach ($sensor->properties as $prop => $propvalue) {
    $sensor_output .= '
    <div class="data">
      <h4 class="parameter-name">' . $sensor->parameters[$prop]['name'] . '</h4>
      <span class="value">' . $propvalue . '</span> <span class="unit">' . $sensor->parameters[$prop]['unit'] . '</span>
    </div>';
  }

  $sensor_output .= '</div>';

  $classes = '';

  if((time() - $sensor->getLastUpdatedTime()) > 300){
    $classes = ' warning';
  }

  $path = '/garden';

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Sensors</title>

    <meta charset="utf-8" />
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300;400;700&display=swap" rel="stylesheet"> 
    <link type="text/css" rel="stylesheet" href="css/styles.css" media="all" />

    <script type="text/javascript"  src="<?php print $path; ?>/js/jquery.min.js"></script>
    <script type="text/javascript"  src="<?php print $path; ?>/js/jquery.flip.min.js"></script>
    <script type="text/javascript"  src="<?php print $path; ?>/js/jquery.form.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script language="javascript" type="text/javascript" src="<?php print $path; ?>/js/flot/jquery.flot.js"></script>
    <script language="javascript" type="text/javascript" src="<?php print $path; ?>/js/flot/jquery.flot.time.js"></script>
    <script language="javascript" type="text/javascript" src="<?php print $path; ?>/js/flot/jquery.flot.selection.js"></script>
    <script language="javascript" type="text/javascript" src="<?php print $path; ?>/js/flot/jquery.flot.crosshair.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    <?php if(isLoggedUser()){ ?>
    <script src="./js/jquery.flip.min.js"></script>
    <script src="./js/jquery.form.min.js"></script>
    <?php } ?>

    <script type="text/javascript" src="./js/sensors.js"></script>
  </head>
  <body>
    <?php include './menu.php'; ?>

    <div id="sensor-card">
      <div class="front"> 
        <div class="last-data">
          <?php print $sensor_output; ?>
        </div>
      </div>
      <div class="back">
        <div class="sensor-edit-form">
          <form action="<?php print ($settings['base_path'])?>api/postSensorConfig.php" id="sensor-edit-form" method="GET">
            <h3>Edit sensor properties: </h3> <img src="<?php print ($settings['base_path'])?>/images/icons/view-icon.png" id="back-to-data-button" />
            <input type="hidden" name="id" value="<?php print $sensor->getId(); ?>" />
            <div class="field">
              <label for="sensor_name">Name</label>
              <input type="text" name="name" value="<?php print $sensor->getName(); ?>" />
            </div>
            <div class="field">
              <label for="props">Properties</label><br />
              <?php

                foreach ($sensor->parameters as $prop => $propvalue) {
                  $checked = '';
                  if(isset($sensor->properties[$prop])) {
                    $checked = 'checked="checked"';
                  }
                  print('<input type="checkbox" name="props[' . $prop . ']" value="' . $prop . '" ' . $checked . '  /> ' . $propvalue['name'] . '<br />');   
                }
              ?>
            </div>

            <div class="field">

              <label for="period">Save data every</label> <select name="period">
              <?php
                $period = $sensor->getPeriod();
                for($i = 1 ; $i < 11 ; $i++) {
                  $period == $i ? $selected = ' selected="selected"' : $selected = '';
                  print('<option value="' . $i . '" ' . $selected . '>' . $i . '</option>');
                }
              ?>
              </select> minutes

            </div>
            <div class="form-button"><input type="submit" value="Update sensor config"/></div>
            <div class="infotext"></div>
          </form>
        </div>
      </div>
    </div>



    <div class="graphs">
      <form id="graph-period"><div class="field">From: <input type="text" class="datepicker from" /></div><div class="field">To: <input type="text" class="datepicker to" /></div><span class="submit-button tables">Update charts</span><A href="<?php print ($settings['base_path'])?>/api/getSensorParamData.php" class="submit-button json">Download JSON</A><A href="<?php print ($settings['base_path'])?>/api/getSensorParamDataCSV.php" class="submit-button csv">Download CVS</A></form>
      <?php
        foreach ($sensor->properties as $key => $value) {
          $machinename = $key;
          $humanname = $sensor->parameters[$key]['name'];
          $units = $sensor->parameters[$key]['unit'];
          $color = $sensor->parameters[$key]['color'];
      ?>
        <div class="parameter">
          <div class="tabs-container <?php print($machinename); ?>">
          <?php print($humanname) ?>
            <div class="tab"></div><div class="tab"></div><div class="tab"></div>
          </div>
          <div class="graph <?php print($machinename); ?>" data-param-name="<?php print($humanname); ?>" data-unit="<?php print($units); ?>" data-sid="<?php print($sensor->getId()); ?>" data-color="<?php print($color); ?>" data-parameter="<?php print($machinename); ?>" id="<?php print($machinename); ?>-graph">
          </div>
        </div>
      <?php
        }
      ?>
    </div>

    <div class="last-values">
      <?php //print($datatable); ?>
    </div>

  </body>
</html>