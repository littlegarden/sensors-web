      <div class="menu">
        <div id="menu-inner">
          <div class="menu-item logo"><a href="<?php print ($settings['base_path'])?>"><img src="./images/logo.png" /></a></div>
          <div class="menu-item"><a href="<?php print ($settings['base_path'])?>" class="text-link">Sensor index</a></div>
          <?php
            if(userIsAdmin()) {
              print('<div class="admin-link-container"><a href="' . $settings['base_path'] . 'admin.php" class="admin-link"><span></span></a></div>');
            }
          ?>
          <?php
            if(isLoggedUser()){
              print('<div class="log-out-link-container"><a href="' . $settings['base_path'] . 'logout.php" class="logout"><span></span></a></div>');
            }
            else {
              print('<div class="log-in-link-container"><a href="' . $settings['base_path'] . 'login.php" class="login"><span></span></a></div>');            
            }
          ?>
        </div>
      </div>