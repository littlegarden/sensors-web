<?php
  session_start();

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';


  if(!isset($_SESSION['user'])){
    header('Location: ../login.php');
  }

  else {
    $user = new User($_SESSION['user']);
  }


  if(isset($_GET['sid'])) {
    $sensor = new Sensor($_GET['sid']);
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=file.csv");
    header("Pragma: no-cache");
    header("Expires: 0");

    isset($_GET['from']) ? $from = $_GET['from'] : $from = null;
    isset($_GET['to']) ? $to = $_GET['to'] : $to = null;

    $data = $sensor->getLastValuesCSV($_GET['param'], $from, $to);
    print($data);
  }

?>