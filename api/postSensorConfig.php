<?php
  session_start();
  //$_SESSION['user'] = 'javi';

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';

  if(!isset($_SESSION['user'])){
    header('Location: ../login.php');
  }

  $sensor_temp = array(
    'id' => $_GET['id'],
    'name' => $_GET['name'],
    'period' => $_GET['period'],
    'props' => implode('|', $_GET['props']),
  );

  $sensor = new Sensor($sensor_temp['id'], $sensor_temp['name'], $sensor_temp['props'], ($sensor_temp['period'] * 60000));

  $sensor->saveSensorProperties();

  print('New sensor configuration has been saved. Refresh the page to see the changes');
?>