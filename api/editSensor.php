<?php
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE); 

  session_start();

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';


  if(!isset($_SESSION['user'])){
    header('Location: ../login.php');
  }

  else {
    $user = new User($_SESSION['user']);
  }

  if(isset($_GET['sid'])) {
    $sensor = new Sensor($_GET['sid']);
  }

?>

<!DOCTYPE html>
<html>
<head>
  <title>Edit sensor</title>
</head>
<body>
  <div class="main">
    <form action="" method="">
      <div class="field">
        <label for="">Sensor name</label>
        <input type="text" value="<?php print $sensor->getName(); ?>" />
      </div>
      <div class="field">
        <label for="">Properties</label>
        <?php
          foreach ($sensor->parameters as $key => $value) {
            print('<input type="checkbox" name="props" value="' . $key . '"');
            if($value['monitored']) {
              print(' checked="checked"');
            }
            print('  /> ' . $value['name'] . '<br />');
          }
        ?>
      </div>
      <div class="field">
        <label for="">Sensor name</label>
        <input type="text" value="" />
      </div>
    </form>
  </div>
</body>
</html>