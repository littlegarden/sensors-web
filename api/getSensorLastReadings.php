<?php
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';

  if(isset($_GET['sid'])) {
    $sensor = new Sensor($_GET['sid']);

    $data = $sensor->getSensorCurrentData();

    $return_array = array();

    foreach ($sensor->properties as $key => $value) {
      $return_array['props'][$key]['name'] = $sensor->parameters[$key]['name'];
      $return_array['props'][$key]['value'] = $value . $sensor->parameters[$key]['unit'];
      $return_array['props'][$key]['only_value'] = $value;
    }

    $return_array['time'] = date("j.n.Y H:i", $sensor->getLastUpdatedTime());


    header('Content-Type: application/json');

    echo json_encode($return_array, JSON_PRETTY_PRINT);
  }

?>