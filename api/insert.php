<?php

  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE); 

  session_start();

  spl_autoload_register(function ($class_name) {
      include './include/' . $class_name . '.php';
  });

  include './include/functions.php';
  include './include/variables.php';


  if(!isset($_SESSION['user'])){
    header('Location: ./login.php');
  }

  else {
    $user = new User($_SESSION['user']);
  }



?>