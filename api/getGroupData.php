<?php
  

  session_start();
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';


  if(!isset($_SESSION['user'])){
    //header('Location: ../login.php');
  }

  $sensors = array();

  $gid = $_GET['gid'];

  $group = new Group($gid);

  $sensorsInfo = $group->getSensors();

  foreach ($sensorsInfo as $key => $value) {
    $sensor = new Sensor($key);
    $sensor->getSensorCurrentData();
    if($sensor->getLastUpdatedTime() > (time() - (3600 * 24 * 180))) {
      $sensors[] = array(
        'id' => $key,
        'name' => $sensor->getName(),
        'props' => $sensor->properties,
        'time' => date("j.n.Y H:i", $sensor->getLastUpdatedTime()),
      );
    }
  }

  header('Content-Type: application/json');
  echo json_encode($sensors, JSON_PRETTY_PRINT);
?>