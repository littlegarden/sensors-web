<?php
  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';

  if(isset($_GET['sid'])) {
    $sensor = new Sensor($_GET['sid']);

    $data = $sensor->getSensorCurrentData();

    $return_array = array();

    foreach ($sensor->properties as $key => $value) {
      $return_array['props'][$key]['name'] = $sensor->parameters[$key]['name'];
      $return_array['props'][$key]['value'] = $value . $sensor->parameters[$key]['unit'];
    }

    $return_array['time'] = $sensor->getLastUpdatedTime();



    print(json_encode($return_array));
  }

?>