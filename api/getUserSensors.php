<?php
  

  session_start();

  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include '../include/functions.php';
  include '../include/variables.php';


  if(!isset($_SESSION['user'])){
    //header('Location: ../login.php');
    $_SESSION['user'] = 'javi';
  }

  $user = new User($_SESSION['user']);
  $userSensors = $user->get_user_sensors();

  $sensors = array();


  foreach ($userSensors as $key => $value) {
    $sensor = new Sensor($key);
    $sensor->getSensorCurrentData();
    if($sensor->getLastUpdatedTime() > (time() - (3600 * 24 * 180))) {
      $sensors[$sensor->getLastUpdatedTime()] = array('id' => $key, 'name' => $sensor->getName());
    }
  }

  ksort($sensors);

  header('Content-Type: application/json');
  echo json_encode($sensors, JSON_PRETTY_PRINT);
?>