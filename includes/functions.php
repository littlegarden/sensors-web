<?php
  // CREATE CONNECTION
  require_once('./conn.php');
  function getSensorProperties($sensorID = 1) {
    include('./conn.php');
    require('./includes/variables.php');

    $sensor_properties_sql = "SELECT props FROM sensores WHERE id = " . $sensorID;
    $sensor_properties = mysqli_query($con, $sensor_properties_sql);
    $record = mysqli_fetch_array($sensor_properties);
    $parameters =  explode( "|" , $record[0]);
    $props = $property_names;
    foreach ($props as $key => $value) {
      if(!in_array($key, $parameters)) {
        unset($props[$key]);
      }
    }
    mysqli_close($con);
    return $props;
  }

  function getCurrentConditions($sensorid = 1, $props = array()) {
    include('./conn.php');
    $fields = "";
    $fieldnames = "";
    foreach ($props as $key => $value) {
      $fields .= "`" . $key . "`, ";
      $fieldnames .= '<th>' . strtoupper($value['name']['EN']) . '</th>';
    }

    $data_table = "<div class='table-container'><h2>Sensor " . $sensorid . "</h2><table class='sensor-last-values'><thead><tr><th>TIME</th>" . $fieldnames . "</tr></thead><tbody>";

    $sensorresults = mysqli_query($con, "SELECT " . $fields . " time FROM data WHERE sensor = " . $sensorid . " ORDER BY time DESC LIMIT 0,60");

    // Last record (First in the row)
    $current_conditions = '<div id="current-data">';
    $sensordata = $sensorresults->fetch_array(MYSQLI_ASSOC);

    $data_table .= "<tr><td>" . date("j.n H:i:s", $sensordata['time'] + 28800) . "</td>";

    foreach ($props as $prop=>$info) {
      $data_table .= "<td>" . $sensordata[$prop] . "</td>";
      $current_conditions .= '<div class="current-data ' . $prop . '"><span class="title">' . $info['name']['EN'] . '</span><br /><span class="value">' . $sensordata[$prop] . '</span><span class="units">' . $info['units'] . '</span></div>';
    }
    $current_conditions .= '<div class="lastupdatedinfo">Last updated on: ' . date("j.n.Y H:i", $sensordata['time'] + 28800) . '</div></div>';
    $data_table .= "</tr>";

    while ($sensordata = $sensorresults->fetch_array(MYSQLI_NUM)){
      $data_table .= "<tr><td>" . date("j.n H:i:s", $sensordata[count($props)] + 28800) . "</td>";
      for ($i = 0; $i < count($props); $i++) {
        $data_table .= "<td>" . $sensordata[$i] . "</td>";
      }
      $data_table .= "</tr>";
    }
    $data_table .= "</tbody></table></div>";
    mysqli_close($con);
    return array($data_table, $current_conditions);
  }

  function lastUpdatedOn($sensorid = 1){
    include('./conn.php');
    mysqli_close($con);
    return $date;

  }

?>