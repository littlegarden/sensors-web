<?php
  // DEFINE PROPERTIES NAMES
  $property_names = array(
    'temp' => array(
      'name' => array(
        'FI' => 'Lämpötila',
        'EN' => 'Air Temperature',
        'ES' => 'Temperatura',
        'short' => 'Temperature',
      ),
      'units' => '°C',
      'functions' => array(
        'Values' => 'drawTemp',
        'Daily Values' => 'drawTempDailyValues',
      ),
    ),
    'hum' => array(
      'name' => array(
        'FI' => 'Ilman kosteus',
        'EN' => 'Air Humidity',
        'ES' => 'Humedad',
        'short' => 'Humidity',
      ),
      'units' => '%',
      'functions' => array(
        'Values' => 'drawHum',
        'Daily Values' => 'drawHumDailyValues',
      ),
    ),
    'soiltemp' => array(
      'name' => array(
        'FI' => 'Mullan lämpötila',
        'EN' => 'Soil Temperature',
        'ES' => 'Soil Temperature',
        'short' => 'Soil temp.',
      ),
      'units' => '° C',
      'functions' => array(
        'Values' => 'drawSoilTemp',
      ),
    ),
    'soilhum' => array(
      'name' => array(
        'FI' => 'Mullan kosteus',
        'EN' => 'Soil Humidity',
        'ES' => 'Soil Humidity',
        'short' => 'Soil hum.',
      ),
      'units' => '%',
      'functions' => array(
        'Values' => 'drawSoilHum',
      ),
    ),
    'press' => array(
      'name' => array(
        'FI' => 'Atmospheric pressure',
        'EN' => 'Atmospheric pressure',
        'ES' => 'Atmospheric pressure',
        'short' => 'Pressure',
      ),
      'units' => 'mbar',
      'functions' => array(
        'Values' => 'drawPress',
      ),
    ),
    'weight' => array(
      'name' => array(
        'FI' => 'Water reservoir',
        'EN' => 'Water reservoir',
        'ES' => 'Water reservoir',
        'short' => 'Water',
      ),
      'units' => 'liters',
      'functions' => array(
        'Values' => 'drawWeight',
      ),
    ),
    'voltage' => array(
      'name' => array(
        'FI' => 'Voltage',
        'EN' => 'Voltage',
        'ES' => 'Voltage',
        'short' => 'Voltage',
      ),
      'units' => 'V',
      'functions' => array(
        'Values' => 'drawVoltage',
      ),
    ),
    'co2' => array(
      'name' => array(
        'FI' => 'Hiilidioksiidi',
        'EN' => 'Carbon dioxide',
        'ES' => 'Dióxido de carbono',
        'short' => 'CO2',
      ),
      'units' => 'ppm',
      'functions' => array(
        'Values' => 'drawCO2',
      ),
    ),
  );

  $watering_length = 120000;
?>