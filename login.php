<?php
  session_start();
  $errormsg = '';
  //phpinfo();


  spl_autoload_register(function ($class_name) {
      include './include/' . $class_name . '.php';
  });

  include './include/functions.php';
  include './include/variables.php';


  if( (isset($_POST['user']) && isset($_POST['passwd'])) && $_POST['user'] != '') {
    $query = "SELECT `id`, `password`, `name` FROM `users` WHERE `name` LIKE '" . $_POST['user'] . "'";
    $results = db_query($query);
    if(mysqli_num_rows($results) == 0) {
      $errormsg = '<p>The user name doesn\'t exist. Who the hell are you?</p>';
    }
    else {
      $row = mysqli_fetch_array($results);
      if($row['password'] != $_POST['passwd']) {
        $errormsg = '<p>The password is wrong. Sorry.</p>';
      }
    }
    if($errormsg == '') {
      $_SESSION["user"] = $_POST["user"];
      header('Location: ./index.php');
    }
  }

?>

<html>
  <head>
    <title>Sensors</title>
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
      @import url("./css/main.css");
    </style>
    <link type="text/css" rel="stylesheet" href="css/styles.css" media="all" />
  </head>
  <body class="login">
    <div class="content-container">
    <!-- <div><img src="./images/logo.png" /></div> -->
      <div class="logo-title">Sensors</div>
      <div class="error-div"><?php print($errormsg); ?></div>
      <form action="./login.php" method="POST">
        <div class="field">
          <input type="text" name="user" value="Username" onclick="if(this.value == 'Username') {this.value = ''};" onblur="if(this.value == '') {this.value = 'Username'};"/>
        </div>
        <div class="field">
          <input type="password" name="passwd" value="Password" onclick="if(this.value == 'Password') {this.value = ''};" onblur="if(this.value == '') {this.value = 'Password'};"  />
        </div>
        <div class="centered">
          <input type="submit" value="Log in">
        </div>
      </form>
    </div>
  </body>
</html>