<?php
  session_start();

  /*
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);
  */

  spl_autoload_register(function ($class_name) {
      include './include/' . $class_name . '.php';
  });

  include './include/functions.php';
  include './include/variables.php';


  if(!isset($_SESSION['user'])){
    header('Location: ./login.php');
  }

 if(!userIsAdmin()) {
    header('Location: ./index.php');
  }

  $user = new User($_SESSION['user']);


?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="refresh" content="120">
    <title>Sensors</title>
    <link type="text/css" rel="stylesheet" href="css/styles.css" media="all" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
  </head>
  <body class="admin">
    <?php include './menu.php'; ?>
    <?php include './include/forms/addUser.php'; ?>      
  </body>
</html>