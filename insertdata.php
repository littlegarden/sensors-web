<?php

  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE); 

  session_start();

  spl_autoload_register(function ($class_name) {
      include '../include/' . $class_name . '.php';
  });

  include './include/functions.php';
  include './include/variables.php';

   
  if(!isset($_ENV["REMOTE_ADDR"])) {
    $IP = '127.1.1.0';
  }
  else {
    $IP = $_ENV["REMOTE_ADDR"];
  }

  if(!isset($_GET['hash'])) {
    $hash = 'test';
  }
  else {
    $hash = $_GET['hash'];
  }



  // CHECK IF THE DATA IS COMING FROM A TRUSTABLE SOURCE
  if($hash == 'javigarden') {
    $weight = 0;
    $weightvalue = 0;
    isset($_GET['st']) ? $st = $_GET['st'] : $st = 0.0;
    isset($_GET['sh']) ? $sh = $_GET['sh'] : $sh = 1023;
    isset($_GET['p']) ? $press = $_GET['p'] : $press = 0;
    isset($_GET['v']) ? $voltage = $_GET['v'] : $voltage = 0;
    isset($_GET['co2']) ? $co2 = $_GET['co2'] : $co2 = 0;
    if(isset($_GET['w']) && $_GET['w'] != 0) {
      $weightvalue = $_GET['w'];
      $weight = $weightvalue - 8420741;
      if($weight < 0) {
        $weight = 0;
      }
      //$weight = number_format($weight / 13037.0530209618, 2, "." ,"");
      $weight = number_format($weight / 6175.446167824010529566024299, 2, "." ,"");
      
    }
    $sh = 1023 - $sh;
    $sh = ($sh * 100) / 1023;

    if($co2 > 0 && $co2 < 320) {
      $extra1 = rand ( 450 , 550 );
      $extra2 = rand ( 0 , 99 );
      //$co2 = $extra1 . '.' . $extra2;
    }

    if($press == "") {
      $press = 0;
    }

    // DATABASE QUERY
    $time = time();
    $finntime = $time + 25200;
    $year = date("Y", $finntime);
    $month = date("n", $finntime);
    $day = date("j", $finntime);
    $hour = date("H", $finntime);

    if($_GET['s'] == 8) {
      $_GET['s'] = 9;
    }

    // Activate the connection alarm
    $res = db_query("UPDATE sensors set alarm_active = 1 WHERE id = " . $_GET['s']);

    $SQL = "INSERT INTO data(`sensor`, `temp`, `hum`, `soiltemp`, `soilhum`, `press`, `weight`, `weightvalue`, `voltage`, `co2`, `time`, `IP`, `year`, `month`, `day`, `hour`) values (" . $_GET['s'] . ", " . $_GET['t'] . ", " . $_GET['h'] . ", " . $st . ", " . $sh . ", " . $press . ", " . $weight . ", " . $weightvalue . ", " . $voltage . ", " . $co2 . ", " . $finntime . ", '" . $IP . "', " . $year . ", " . $month . ", " . $day . ", " . $hour . ")";

    db_query($SQL);

    $SQL = "UPDATE `last_data` set `temp` = " . $_GET['t'] . ", `hum` = " . $_GET['h'] . ", `soiltemp` = " . $st . ", `soilhum` = " . $sh . ", `press` = " . $press . ", `weight` = " . $weight . ", `weightvalue` = " . $weightvalue . ", `voltage` = " . $voltage . ", `co2` = " . $co2 . ", `time` = " . $finntime . ", `IP` = '" . $_ENV["REMOTE_ADDR"] . "' WHERE `sensor` = " . $_GET['s'];
    
    db_query($SQL);
  }

  header('Content-Type: application/json');

  $sensors = array(
    1 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    2 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    3 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    4 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    5 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    6 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    7 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    8 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    9 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    10 => array('interval' => 300000, 'maxHum' => 85, 'minHum' => 80, 'minPpm' => 500, 'maxPpm' => 900),
    11 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    12 => array('interval' => 300000, 'maxHum' => 110, 'minHum' => 100, 'minPpm' => 500, 'maxPpm' => 700),
    13 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    14 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    15 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    16 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    17 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    18 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    19 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    20 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    21 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
    22 => array('interval' => 300000, 'maxHum' => 90, 'minHum' => 70, 'minPpm' => 500, 'maxPpm' => 900),
  );

  $sensor_settings = $sensors[$_GET['s']];


  $SQL = "SELECT period FROM `sensors` WHERE `id` = " . $_GET['s'];
  $results = db_query($SQL);
  while ($row = $results->fetch_assoc()) {
    $sensor_period = $row["period"];
    if($sensor_period < 1000) {
      $sensor_period = $sensor_period * 60000;
    }
    $sensor_settings['interval'] = $sensor_period;
  }

  print(json_encode($sensor_settings));
?>
