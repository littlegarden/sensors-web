(function($){
  var SENSORS = SENSORS || {};

  SENSORS.graphs = {
    from: null,
    to: null,
    sid: null,
    init: function(){
      var context = SENSORS.graphs;
      context.displayGraphs();
    },
    displayGraphs: function(){

      $('.graphs .graph').each(function(){
        var elem = $(this),
            context = SENSORS.graphs,
            param = elem.attr('data-parameter'),
            sid = elem.attr('data-sid'),
            units = elem.attr('data-unit'),
            human_name = elem.attr('data-param-name'),
            color = elem.attr('data-color'),
            rgb = SENSORS.graphs.hexToRgb(color),
            url = "/garden/api/getSensorParamData.php?sid=" + sid + "&param=" + param;
        if(context.sid == null) {
          context.sid = sid;
        }


        if(context.from != null) {
          url += '&from=' + context.from;
        }

        if(context.to != null) {
          url += '&to=' + context.to;
        }

        console.log(url);


        var jsonData = jQuery.ajax({
          url: url,
            async: false
          }).responseText;

        
        jsonData = JSON.parse(jsonData);

        $.plot("#" + param + "-graph", [{label: human_name + " (" + units + ")", data: jsonData["values"]}], {
            xaxis: {
              mode: "time",
              //timeformat: "%e.%m %H:%m"
              timeformat: "%e.%m"
            },
            yaxis: {
              min: jsonData['min'] * 0.95,
              max: jsonData['max'] * 1.05,
            },
            series: {
              lines: { show: true, fill: true, fillColor: "rgba(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ", 0.3)" },
              points: { show: false, fill: false }
            },
            legend: {
              show: true,
              labelFormatter: null,
              labelBoxBorderColor: "#879976",
              noColumns: 1,
              position: "se",
              margin: 20,
              backgroundColor: "#FFFFFF",
              backgroundOpacity: 0.5,
              container: null,
              sorted: "ascending"
            },
            colors: ["#" + color],
        });
      });
    },
    hexToRgb: function (hex) {
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function(m, r, g, b) {
          return r + r + g + g + b + b;
      });

      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16)
          } : null;
    },
  };

  SENSORS.base = {
    init: function(){
      var context = SENSORS.base;
      context.saveSensorConfig();
    },

    saveSensorConfig: function(){
      //$("#sensor-edit-form").ajaxForm({url: '/api/postSensorConfig.php', type: 'post'});
    },

  };

  $(document).ready(function(){
    $( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });
    SENSORS.graphs.init();
    setTimeout(60000, SENSORS.graphs.init());
    SENSORS.base.init();
    $("#sensor-card").flip({trigger: 'manual'});
    $('.back').css('display', 'block');
    $("#edit-icon").click(function(){
      $("#sensor-card").flip(true);
    });
    $("#back-to-data-button").click(function(){
      $("#sensor-card").flip(false);
    });

    $('FORM#graph-period .submit-button.tables').click(function(){
      var from = $('FORM#graph-period .from').val();
      var to = $('FORM#graph-period .to').val();
      if(from.length > 0) {
        var date_array = from.split(".");
        var d = Date.UTC(date_array[2], date_array[1] - 1, date_array[0]);
        SENSORS.graphs.from = d/1000;
      }
      if(to.length > 0) {
        var date_array = to.split(".");
        var d = Date.UTC(date_array[2], date_array[1] - 1, date_array[0]);
        SENSORS.graphs.to = d/1000;
      }

      $('FORM#graph-period .submit-button.json, FORM#graph-period .submit-button.csv').css('display', 'inline-block');

      SENSORS.graphs.init();
    });

    $('FORM#graph-period .submit-button.csv').click(function(e){
      e.preventDefault();  //stop the browser from following
      window.location.href = $(this).attr('href') + '?sid=' + SENSORS.graphs.sid + '&from=' + SENSORS.graphs.from + '&to=' + SENSORS.graphs.to;
    });


  });


$(document).ready(function() { 
    var options = { 
        target:        '.infotext',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
 
        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
 
        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    }; 
 
    // bind form using 'ajaxForm' 
    $('#sensor-edit-form').ajaxForm(options); 
}); 
  // pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    //alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    //alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.'); 
} 

})(jQuery)