<div class="admin-form-container">
  <div class="form-title">Add new user</div>
  <form method="GET" action="./admin.php" class="admin-form form-add-user">
    <input type="hidden" name="form" value="addUser" />
    <div class="form-field"><label>User name:</label> <input type="text" name="username" /></div>
    <div class="form-field"><label>Password:</label> <input type="password" name="password" /></div>
    <div class="form-field"><label>Role:</label> <select name="role"><option value="0">Site admin</option><option value="1">Sensor viewer</option></select></div>
    <div>Assign sensors to user:
      <?php
        $sensors = getSensorList();
        print_r(count($sensors));
        foreach($sensors as $id => $name) {
          print('<div class="form-field field-select"><label>' . $name . ':</label>  <select name="sensors[' . $id . ']"><option value="0" selected="selected">No rights</option><option value="1" >View sensor data</option><option value="2">Admin sensor</option></select></div>');
        }
      ?>
    </div>
    <div class="form-field"><input type="submit" value="Add user" /></div>
  </form>
</div>