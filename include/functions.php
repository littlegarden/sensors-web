<?php

  $global_vars = array(
    'emails' => array(
      'connalarm' => 'Sensor {sid} has not been working for the last 30 minutes.',
    ),
  );


function db_query($query) {
  include('settings.php');
  // CHECK CONNECTION
  $conn = mysqli_connect($db_config['server'], $db_config['user'], $db_config['passw'], $db_config['database']);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  $result = mysqli_query($conn, $query);
  
  mysqli_close($conn);
  
  return $result;
}



function isloggedUser() {
  return isset($_SESSION['user']);
}

function userIsAdmin() {
  if(!isset($_SESSION['user'])) {
    return false;
  }

  $user = new User($_SESSION['user']);
  $roles = $user->getUserRoles();
  return in_array(0, $roles);
}

function getSensorList() {

  $sensors = array();

  $sqlQuery = "SELECT id, name FROM sensors ORDER BY id ASC";

  $result = db_query($sqlQuery);

  while ($row = $result->fetch_assoc()) {
    $sensors[$row['id']] = $row['name'];
  }

  return $sensors;
}



function checkConnections(){

  $now = time();

  $sqlQuery = "SELECT sensors.id, sensors.name, sensors.alarm_active, max(data.time) as lasttime FROM sensors LEFT JOIN data ON sensors.id = data.sensor GROUP BY sensors.id";

  $result = db_query($sqlQuery);

  while ($row = $result->fetch_assoc()) {

    $lasttime = $row["lasttime"];
    if($lasttime < $now - 3600 && $row['alarm_active']) {
      print('<br  />Send alarm about sensor ' . $row['id'] . '. Last data was sent at ' . date('j.n.Y H.i:s', $lasttime) . ', so later than ' . date('j.n.Y H.i:s', $now - 3600) );
      sendConnAlarm($row['id']);
    }
  }
}

function sendConnAlarm($sid) {
  $emails = array();
  $message = $global_vars['emails']['connalarm'];

  $sqlQuery = "SELECT user_sensor.uid, users.email FROM user_sensor LEFT JOIN users ON users.id = user_sensor.uid WHERE user_sensor.sid = " . $sid . " AND user_sensor.role = 0 AND users.email NOT LIKE ''";

  $result = db_query($sqlQuery);

  while ($row = $result->fetch_assoc()) {
    $emails[] = $row['email'];
  }

  mail(implode(', ', $emails), 'Sensor error connection' , $message);

  $sqlQuery = "UPDATE sensors SET alarm_active = 0 WHERE id = " . $sid;

  $result = db_query($sqlQuery);
}

function load_environmental_parameters() {
  $result = db_query("SELECT `id`, `human_name`, `machine_name`, `unit`, `color` FROM `env_params` ORDER BY `id`");

  $env_params = array();

  while ($row = $result->fetch_assoc()) {
    $env_params[$row["machine_name"]] = array(
      'name' => $row["human_name"],
      'color' => $row["color"],
      'unit' => $row["unit"],
    );
  }

  return $env_params;
}

function array2csv(array &$array) {
  if (count($array) == 0) {
    return null;
  }
  ob_start();
  $df = fopen("php://output", 'w');
  fputcsv($df, array_keys(reset($array)));
  foreach ($array as $row) {
    fputcsv($df, $row);
  }
  fclose($df);
  return ob_get_clean();
}


function getRealIpAddr() {

  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
    $ip=$_SERVER['HTTP_CLIENT_IP'];
  }

  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  }

  else {
    $ip=$_SERVER['REMOTE_ADDR'];
  }

  return $ip;

}


?>