<?php
class Sensor {

    private     $id = null;
    private     $name = null;
    private     $lastUpdated = 1;
    private     $period = null;
    public      $props = '';
    public      $properties = array();
    public      $parameters = array();
    public      $public = false;
    public      $visible = false;


    function __construct($id = null, $name = null, $props = null, $period = null) {

        if($id == null) {
            throw new Exception("You can't create a Sensor object without providing a sensor id. Try new Sensor($id)");
        }

        else {
            $this->id = $id;
        }
        if($name == null || $props == null || $period == null) {
            $this->loadSensorProperties($this->id);
        }
        else {
            $this->name = $name;
            $this->props = $props;
            $this->period = $period;
        }

        $this->parameters = load_environmental_parameters();
    }

    // INTERACTION WITH CLASS PROPERTIES

    public function getId() {
        return $this->id;
    }

    public function getLastUpdatedTime(){
        if($this->lastUpdated > 0) {
            return $this->lastUpdated;
        }
        else {
            return '0';
        }
    }

    public function getLastValuesCSV($param = null, $from = null, $to = null){

        if($from == null || $from == '') {
            $from = time() - (3600 * 24 * 7);
        }
        if($to == null || $to == '') {
            $to = time();
            $from += 7200;
        }

        if($to < $from) {
            $from = $to - (3600 * 24 * 7);
        }

        if($param == null || $param == '') {
            $param = '';
            $params = array();
            foreach ($this->properties as $key => $value) {
                $param .= '`' . $key . '`, ';
                $params[] = $key;
            }
        }
        else {
            $param = '`'. $param .'`,';
            $params = array($param);
        }


        $sqlQuery =  "SELECT `sensor`, " . $param . "`time` FROM data WHERE `sensor` =  ". $this->id . " AND `time` > " . $from . " AND `time` < " . $to . " ORDER BY `time` ASC";


        $result = db_query($sqlQuery);
        
        $data = array();

        $tmp = array('Time');
        foreach ($params as $key => $value) {
            $tmp[] = $value;
        }

        $data['values'][] = $tmp;

        while ($row = $result->fetch_assoc()) {
            $tmp = array(date('j.n.Y H:i:s', $row['time']));
            foreach ($params as $key => $value) {
                $tmp[] = $row[$value];
            }
            $data['values'][] = $tmp;
        }

        $string = '';
        ob_start();
        $df = fopen("php://output", 'w');
        foreach ($data['values'] as $row) {
            fputcsv($df, $row);
        }
       
        fclose($df);
        return ob_get_clean();
    }

    public function getLastValuesJSON($param = null, $from = null, $to = null, $num = null, $jsonEncoded = true){

        if($param == null || $param == '') {
            $param = '';
            $params = array();
            foreach ($this->properties as $key => $value) {
                $param .= $key . ', ';
                $params[] = $key;
            }
        }
        else {
            $params = array($param);
            $param = $param .', ';
        }

        if($num === null || !is_numeric(intval($num))) {
            if($from == null || $from == '') {
                $from = time() - (3600 * 24 * 7);
            }
            if($to == null || $to == '') {
                $to = time();
                $to += 28800;
            }

            if($to < $from) {
                $from = $to - (3600 * 24 * 7);
            }

            $sqlQuery =  "SELECT `sensor`, " . $param . "`time` FROM data WHERE `sensor` =  ". $this->id . " AND `time` > " . $from . " AND `time` < " . $to . " AND `status` = 1 ORDER BY `time` ASC";
            $sqlQuery2 =  "SELECT min(" . $params[0] . ") as min, max(" . $params[0] . ") as max FROM data WHERE `sensor` =  ". $this->id . " AND `status` = 1 AND `time` > " . $from;
        }

        else {
            $sqlQuery =  "SELECT `sensor`, " . $param . "`time` FROM data WHERE `sensor` =  ". $this->id . " AND `status` = 1 ORDER BY `time` DESC LIMIT 0," . intval($num);
            $sqlQuery2 =  "SELECT min(" . $params[0] . ") as min, max(" . $params[0] . ") as max FROM data WHERE time in (SELECT `time` FROM data WHERE `sensor` =  ". $this->id . " AND `status` = 1 ORDER BY `time` DESC LIMIT 0," . intval($num) . ")";
        }

        $result = db_query($sqlQuery);
        
        $data = array();
        $minValue = 100000;
        $maxValue = -100000;

        while ($row = $result->fetch_assoc()) {
            $minValue = min([$minValue, $row[$params[0]]]);
            $maxValue = max([$maxValue, $row[$params[0]]]);
            $data['values'][] = array(($row['time'] . '000' - 18000000), $row[$params[0]]);
        }
        $data['min'] = array($minValue);
        $data['max'] = array($maxValue);

        if($jsonEncoded) {
            return json_encode($data);            
        }

        return $data;

    }

    public function getLastValuesOnly($param, $num) {
        $data = $this->getLastValuesJSON($param, null, null, $num, false);
        $sortedValues = array();
        foreach ($data['values'] as $entry) {
            $sortedValues[''.$entry[0]] = $entry[1];
        }
        ksort($sortedValues);
        $data['values'] = array_values($sortedValues);
        return json_encode($data);
    }

    public function getName() {
        return $this->name;
    }

    public function getPeriod() {
        return $this->period;
    }

    public function getSensorCurrentData(){


        $fields = "";

        foreach ($this->properties as $key => $value) {
            $fields .= '`' . $key . '`, ';
        }

        $sqlQuery = "SELECT " . $fields . " `time` FROM `last_data` WHERE `sensor` = " . $this->id . " ORDER BY `time` DESC LIMIT 0,1";

        $result = db_query($sqlQuery);

        if ($result->num_rows > 0) {

            $data = null;

            while ($row = $result->fetch_assoc()) {

                foreach ($row as $key => $value) {
                    if($key == 'time') {
                        $this->lastUpdated = $value;
                    }
                    else {
                        $this->properties[$key] = $value;
                    }
                }
            }

            return $data;
        }
        else {
            return 'No rows';
        }         
    }

    public function getSensorLastValueList($count) {
        $fields = "";

        foreach ($this->properties as $key => $value) {
            $fields .= '`' . $key . '`, ';
        }

        $sqlQuery = "SELECT " . $fields . " `time`, `id`, `status` FROM `data` WHERE `sensor` = " . $this->id . " AND status = 1 ORDER BY `id` DESC LIMIT 0," . $count;

        $result = db_query($sqlQuery);

        if ($result->num_rows > 0) {

            $data = array();

            while ($row = $result->fetch_assoc()) {
                $data[$row['time']] = array();
                foreach ($row as $key => $value) {
                    if($key != 'time') {
                        $data[$row['time']][$key] = $value;
                    }
                }
            }

            $cells = '';

            $first_element = array_slice($data, 0, 1);

            foreach ($first_element[0] as $key => $value) {
                $cells .= '<th>' . $this->parameters[$key]['name'] . '</th>';
            }

            $output = '<table><thead><tr><th>Time</th>' . $cells . '</th><th></th></tr></thead><tbody>';



            foreach ($data as $key => $value) {
              $output .= '<tr class="status-' . $value['status'] . '"><td>' . date('j.n.Y H:i', $key) . '</td>';
              foreach ($value as $prop => $propvalue) {
                if($prop != 'id' && $prop != 'status') {
                    $output .= '<td>' . $propvalue . '</td>';
                }

              }
              if(isset($_SESSION['user'])){
                $output .= '<td><a href="./api/deleteSensorData.php?eid=' . $value['id'] . '&sid=' . $this->id . '"><img src="./images/icons/trash.png" style="width: 20px; height: auto;" /></a></td>';
              }

              $output .= '</tr>';
            }


            $output .= '</tbody></table>';

            return $output;
        }
        else {
            return 'No rows';
        } 
    }


    // Load the sensor information from the database
    private function loadSensorProperties(){
    
        $sqlQuery = "SELECT `id`, `name`, `props`, `period`, `public`, `visible` FROM `sensors` WHERE `id` = " . $this->id;

        $result = db_query($sqlQuery);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->id = $row["id"];
                $this->name = $row["name"];
                $this->period = $row["period"];
                $this->public = $row["public"];
                $this->visible = $row["visible"];


                $props = explode("|", $row["props"]);

                foreach ($props as $key => $value) {
                  $this->properties[$value] = "";
                }
            }
        }
    }


    public function removeSensorEntry($eid = null){

        if($eid == null) {
            return null;
        }

        $sqlQuery =  "UPDATE `data` SET `status` = 0 WHERE `id` = " . $eid;
        print_r($sqlQuery);

        $result = db_query($sqlQuery);

        return null;
    }

    // Save the sensor information on the database
    public function saveSensorProperties(){

        $sqlQuery = "UPDATE `sensors` SET `name` = '" . $this->name . "', `props` = '" . $this->props . "', `period` = '" . $this->period . "' WHERE `id` = " . $this->id;

        $result = db_query($sqlQuery);
    }

    public function setName($name) {
        $this->name = $name;
    }
    public function setPeriod($period) {
        $this->period = $period;
    }
}

?>