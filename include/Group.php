<?php 

class Group {

	private     $gid = null;
    private     $name = null;
    private     $sensors = array();

    function __construct($gid = null, $name = null, $props = null, $period = null) {

        if($gid == null) {
            throw new Exception("You can't create a Group object without providing a group id. Try new Group($id)");
        }

        else {
            $this->gid = $gid;
        }

        $this->name = $name;
        $this->sensors = self::getGroupSensors($gid);
    }

    private static function getGroupSensors($gid) {

    	$sensors = array();

        $sqlQuery =  "SELECT `sid` FROM `sensor_group` WHERE `gid` =  ". $gid . " ORDER BY `sid` ASC";

        $result = db_query($sqlQuery);
        

        while ($row = $result->fetch_assoc()) {
            $sensors[$row['sid']] = [];
        }

        return $sensors;
    }

    public function getSensors() {
    	return $this->sensors;
    }
}
?>