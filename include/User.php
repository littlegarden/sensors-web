<?php
class User {

    private $uid = null;
    private $name = null;
    private $sensors = array();
    private $roles = array();


    function __construct($id = null) {

        if($id == null) {
            throw new Exception("You can't create a User object without providing a sensor id. Try new User($id)");
        }
        
        $this->user_load($id);

    }

    // INTERACTION WITH CLASS PROPERTIES
    private function getName() {
        return $this->name;
    }

    private function setName($name) {
        $this->name = $name;
    }


    // Load the sensor information from the database
    private function user_load($id){

        if(is_numeric($id)) {
            $sqlQuery = "SELECT `id`, `name`, `password`, `role` FROM `users` WHERE `id` = '" . $id . "'";
        }
        else {
            $sqlQuery = "SELECT `id`, `name`, `password`, `role` FROM `users` WHERE `name` = '" . $id . "'";
        }        


        $result = db_query($sqlQuery);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->uid = $row["id"];
                $this->name = $row["name"];
                $this->roles = array($row["name"]);
            }
        }

        else {
          print('Shit');
        }
    }

    // Save the sensor information on the database
    private function user_save() {
        $sqlQuery = "UPDATE `users` SET `name` = '" . $this->name . " WHERE `id` = " . $this->uid;
        $result = db_query($sqlQuery);
        $conn->close();
    }

    public function getUserRoles() {
        return $this->roles;
    }

    //
    public function get_user_sensors() {

        $sqlQuery = "SELECT `sid` FROM `user_sensor` WHERE `uid` = " . $this->uid . " ORDER BY `sid` ASC";

        $result = db_query($sqlQuery);

        if ($result->num_rows > 0) {

            $data = array();

            while ($row = $result->fetch_assoc()) {
                $data[$row['sid']] = array();
            }

            return $data;
        }

        return array();
         
    }
}

?>