<?php
  session_start();
  spl_autoload_register(function ($class_name) {
      include './include/' . $class_name . '.php';
  });

  include './include/functions.php';
  include './include/variables.php';


  if(!isset($_SESSION['user'])){
    header('Location: ./login.php');
  }

  $user = new User($_SESSION['user']);
  $sensors = $user->get_user_sensors();

  foreach ($sensors as $key => $value) {
    $sensors[$key] = new Sensor($key);
  }

  $sensors_sorted = array();

  foreach ($sensors as $key => $value) {
    $value->getSensorCurrentData();
    $sensors_sorted[$value->getLastUpdatedTime()] = $value;
  }

  krsort($sensors_sorted);

  $sensors_output = array();

  foreach ($sensors_sorted as $key => $value) {

    if($key > (time() - (3600 * 24 * 180))) {
      $sensors_output[$key] = '<h3 class="sensor-name"><a href="' . $settings['base_path'] . 'sensor.php?sid=' . $value->getId() . '">' . $value->getName() . '</a></h3>';
      $sensors_output[$key] .= '<span class="last-updated">' . date('j.n H:i', $value->getLastUpdatedTime()) . '</span>';
      $sensors_output[$key] .= '<div class="data">';
      foreach ($value->properties as $prop => $propvalue) {
        $sensors_output[$key] .= "<p><label>" . $property_names[$prop]['name']['EN'] . ":</label> " . $propvalue . " " . $property_names[$prop]['units'] . "</p>";
      }

      $sensors_output[$key] .= '</div>';


      $classes = '';

      if((time() - $value->getLastUpdatedTime()) > 300){
        $classes = ' warning';
      }

      $sensors_output[$key] = '<div class="sensor sensor-' . $value->getId() . $classes . ' ">' . $sensors_output[$key] . '</div>';
    }
  }



?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="refresh" content="120">
    <title>Sensors</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300;400;700&display=swap" rel="stylesheet"> 
    <link type="text/css" rel="stylesheet" href="css/styles.css" media="all" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />

  </head>
<body>
<?php include './menu.php'; ?>
<div class="sensor-list">
  <?php
    foreach ($sensors_output as $key => $value) {
      print($value);
    }
  ?>
</div>

</body>
</html>